variable "prefix" {
  default = "fetus-d"
}

variable "project" {
  default = "fetus-devops"
}

variable "contact" {
  default = "a0937027831@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "fetus-devops-bastion"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "279772382515.dkr.ecr.ap-northeast-1.amazonaws.com/fetus-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "279772382515.dkr.ecr.ap-northeast-1.amazonaws.com/fetus-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

# ... existing code ...

variable "dns_zone_name" {
  description = "Domain name"
  default     = "fetusdesign.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "www.production"
    staging    = "www"
    dev        = "www.dev"
  }
}
