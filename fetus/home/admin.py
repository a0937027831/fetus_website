from django.contrib import admin
from .models import Banner,CustomerContact

# Register your models here.

admin.site.register(Banner)
admin.site.register(CustomerContact)